

function firstlevel_init(){
  // initialize level 1

  console.log("This players are logged in:");
  for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
    console.log(MTLG.getPlayer(i));
  }

  console.log("Thie are the available game options:");
  console.log(MTLG.getOptions());

  // esit the game.settings.js
  console.log("Thie are the available game settings:");
  console.log(MTLG.getSettings());

  drawLevel1();
}

// check wether level 1 is choosen or not
function checkLevel1(gameState){
  if(gameState && gameState.nextLevel && gameState.nextLevel == "level1"){
    return 1;
  }
  return 0;
}


function drawLevel1(){
  var stage = MTLG.getStageContainer();
  var options = MTLG.getOptions();

  MTLG.setBackgroundColor("white");

  console.log("Level 1 started.");
  MTLG.eyetracking.initialize();

  var container = new createjs.Container();
  container.addChild(shape);
  container.addChild(circle);
  container.x = options.width * 1/4;
  container.y = options.height * 1/2;
  //stage.addChild(container);

  // create rectangle
  var shape = new createjs.Shape();
  shape.graphics.beginFill("#22b11b").drawRect(0, 0, 100, 100);
  shape.x = options.width * 1/4;
  shape.y = options.height * 1/2;
  shape.name = "box";
  shape.setBounds(0, 0, 100, 100);
  stage.addChild(shape);

  // create circle
  var circle = new createjs.Shape();
  circle.graphics.beginFill("#ffc200").drawCircle(0, 0, 100);
  circle.x = options.width * 3/4;
  circle.y = options.height * 1/2;
  circle.name = "circle";
  circle.setBounds(-100, -100, 200, 200);
  stage.addChild(circle);


  // console.log(container, container.getBounds());

  MTLG.eyetracking.createArea("top_left", 0, 0, 500, 500, null);
  MTLG.eyetracking.createArea("bottum_right", MTLG.getOptions().width-500, MTLG.getOptions().height-500, 500, 500, null);

  MTLG.eyetracking.initPlayers();
  MTLG.eyetracking.visualizeGazeOfAllPlayers();


  MTLG.eyetracking.registerListenerForArea("top_left", () => console.log("listener for area top_left", arguments), 1000, 1000);
  MTLG.eyetracking.registerListenerForObject(circle, () => console.log("listener for circle", arguments), 1000, 1000);
}
